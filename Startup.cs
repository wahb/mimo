﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using MimoAPI.Models;
using Microsoft.Extensions.DependencyInjection;

[assembly: OwinStartup(typeof(MimoAPI.Startup))]

namespace MimoAPI
{
    public partial class Startup
    {
        public Startup()
        {
            using (var client = new MimoContext())
            {
                client.Database.EnsureCreated();
            }
        }

        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }

        public void ConfigureServices(IServiceCollection services)
        {

            services.AddEntityFrameworkSqlite().AddDbContext<MimoContext>();

            // MVC and JSON
            services.AddMvc().AddJsonOptions(options =>
            {
                options.SerializerSettings.ContractResolver = new DefaultContractResolver();
            });

            services.AddSingleton<IRepository<Course>, CourseRepository>();
            services.AddSingleton<IRepository<Chapter>, ChapterRepository>();
            services.AddSingleton<IRepository<Lesson>, LessonRepository>();
            services.AddSingleton<IRepository<User>, UserRepository>();
            services.AddSingleton<IRepository<Activity>, ActivityRepository>();
            services.AddSingleton<IRepository<Achievement>, AchievementRepository>();
        }
    }
}
