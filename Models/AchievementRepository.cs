﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace MimoAPI.Models
{
    public class AchievementRepository : IRepository<Achievement>
    {
        private readonly MimoContext _context;

        public AchievementRepository(MimoContext context)
        {
            _context = context;
        }

        [HttpPost]
        public void create(Achievement record)
        {
            try
            {
                _context.Achievements.Add(record);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpDelete]
        public void destroy(int id)
        {
            var record = _context.Achievements.First(t => t.id == id);
            if (record != null)
            {
                try
                {
                    _context.Achievements.Remove(record);
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw e;
                }

            }
        }

        [HttpGet]
        public Achievement get(int id)
        {
            return _context.Achievements.First(t => t.id == id);
        }

        [HttpPut]
        public void update(Achievement record)
        {
            if (record != null)
            {
                try
                {
                    _context.Achievements.Update(record);
                    _context.SaveChanges();
                } catch (Exception e)
                {
                    throw e;
                }
            }
        }
    }
}