﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MimoAPI.Models
{
    public interface IRepository<T>
    {
        void create(T record);
        T get(int id);
        void destroy(int id);
        void update(T record);
    }
}
