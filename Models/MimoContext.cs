﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MimoAPI.Models
{
    public class MimoContext : DbContext
    {
        public DbSet<Course> Courses { get; set; }
        public DbSet<Chapter> Chapters { get; set; }
        public DbSet<Lesson> Lessons { get; set; }
        public DbSet<Activity> Activities { get; set; }
        public DbSet<Achievement> Achievements { get; set; }
        public DbSet<User> Users { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlite("Filename=./mimo.db");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Chapter>()
                .HasOne(chapter => chapter.course)
                .WithMany(course => course.Chapters)
                .HasForeignKey(chapter => chapter.course_id);

            modelBuilder.Entity<Lesson>()
                .HasOne(lesson => lesson.chapter)
                .WithMany(chapter => chapter.lessons)
                .HasForeignKey(lesson => lesson.chapter_id);
        }
    }
}