﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MimoAPI.Models
{
    public class Achievement
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key, Column(Order = 0)]
        public int id { get; set; }

        [Required]
        public User fromUser { get; set; }

        public string description { get; set; }
        public string type { get; set; } // [lesson, chapter, course]
        public int goal { get; set; }
        public int current { get; set; }
        public bool isCompleted { get; set; } // true if goal == current
        public string comment { get; set; }
    }
}