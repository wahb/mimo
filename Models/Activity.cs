﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MimoAPI.Models
{
    public class Activity
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key, Column(Order = 0)]
        public int id { get; set; }

        [Required]
        public User fromUser { get; set; }
        public string type { get; set; } // [lesson, chapter, course]
        public Course course { get; set; }
        public Chapter chapter { get; set; }
        public Lesson lesson { get; set; }
        public DateTime start { get; set; }
        public DateTime end { get; set; }
    }
}