﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace MimoAPI.Models
{
    public class LessonRepository : IRepository<Lesson>
    {
        private readonly MimoContext _context;

        public LessonRepository(MimoContext context)
        {
            _context = context;
        }

        [HttpPost]
        public void create(Lesson record)
        {
            try
            {
                _context.Lessons.Add(record);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpDelete]
        public void destroy(int id)
        {
            var record = _context.Lessons.First(t => t.id == id);
            if (record != null)
            {
                try
                {
                    _context.Lessons.Remove(record);
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw e;
                }

            }
        }

        [HttpGet]
        public Lesson get(int id)
        {
            return _context.Lessons.First(t => t.id == id);
        }

        [HttpPut]
        public void update(Lesson record)
        {
            throw new NotImplementedException();
        }
    }
}