﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace MimoAPI.Models
{
    public class UserRepository : IRepository<User>
    {
        private readonly MimoContext _context;

        public UserRepository(MimoContext context)
        {
            _context = context;
        }

        [HttpPost]
        public void create(User record)
        {
            try
            {
                _context.Users.Add(record);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpDelete]
        public void destroy(int id)
        {
            var record = _context.Users.First(t => t.id == id);
            if (record != null)
            {
                try
                {
                    _context.Users.Remove(record);
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw e;
                }

            }
        }

        [HttpGet]
        public User get(int id)
        {
            return _context.Users.First(t => t.id == id);
        }

        [HttpPut]
        public void update(User record)
        {
            throw new NotImplementedException();
        }
    }
}