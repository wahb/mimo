﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace MimoAPI.Models
{
    public class ActivityRepository : IRepository<Activity>
    {
        private readonly MimoContext _context;

        public ActivityRepository(MimoContext context)
        {
            _context = context;
        }

        [HttpPost]
        public void create(Activity record)
        {
            try
            {
                _context.Activities.Add(record);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpDelete]
        public void destroy(int id)
        {
            var record = _context.Activities.First(t => t.id == id);
            if (record != null)
            {
                try
                {
                    _context.Activities.Remove(record);
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw e;
                }

            }
        }

        [HttpGet]
        public Activity get(int id)
        {
            return _context.Activities.First(t => t.id == id);
        }

        [HttpPut]
        public void update(Activity record)
        {
            throw new NotImplementedException();
        }
    }
}