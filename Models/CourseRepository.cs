﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace MimoAPI.Models
{
    public class CourseRepository : IRepository<Course>
    {
            private readonly MimoContext _context;

            public CourseRepository(MimoContext context)
            {
                _context = context;
            }

            [HttpPost]
            public void create(Course record)
            {
                try {
                    _context.Courses.Add(record);
                    _context.SaveChanges();
                } catch(Exception e)
                {
                    throw e;
                }
            }

            [HttpDelete]
            public void destroy(int id)
            {
                var record = _context.Courses.First(t => t.id == id);
                if (record != null)
                {
                    try
                    {
                        _context.Courses.Remove(record);
                        _context.SaveChanges();
                    } catch (Exception e)
                    {
                        throw e;
                    }
                
                }
            }

            [HttpGet]
            public Course get(int id)
            {
                return _context.Courses.First(t => t.id == id);
            }

            [HttpPut]
            public void update(Course record)
            {
                throw new NotImplementedException();
            }
    }
}