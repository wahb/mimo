﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace MimoAPI.Models
{
    public class ChapterRepository : IRepository<Chapter>
    {
        private readonly MimoContext _context;
 
        public ChapterRepository(MimoContext context)
        {
            _context = context;
        }

        [HttpPost]
        public void create(Chapter record)
        {
            try
            {
                _context.Chapters.Add(record);
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                throw e;
            }
        }

        [HttpDelete]
        public void destroy(int id)
        {
            var record = _context.Chapters.First(t => t.id == id);
            if (record != null)
            {
                try
                {
                    _context.Chapters.Remove(record);
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    throw e;
                }

            }
        }

        [HttpGet]
        public Chapter get(int id)
        {
            return _context.Chapters.First(t => t.id == id);
        }

        [HttpPut]
        public void update(Chapter record)
        {
            throw new NotImplementedException();
        }
    }
}