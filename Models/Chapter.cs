﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace MimoAPI.Models
{
    public class Chapter
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Key, Column(Order = 0)]
        public int id { get; set; }

        [Required]
        public string name { get; set; }

        public int lessonsCount { get; set; }
        public List<Lesson> lessons { get; set; }

        public int course_id { get; set;  }
        public Course course { get; set; }
    }
}