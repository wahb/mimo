﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using MimoAPI.Models;
using Microsoft.EntityFrameworkCore;

namespace MimoAPI.Controllers
{
    [Route("api/[controller]")]
    public class API<T>
    {
        private readonly IRepository<T> _repo;
        private readonly string _username = "mimo";
        private readonly string _password = "mimo";

        public API(IRepository<T> repo)
        {
            _repo = repo;
        }

        [HttpPost]
        public void create([FromBody] T record, [FromBody] string username, [FromBody] string password)
        {
            if (_username == username && _password == password)
            {
                _repo.create(record);
            }
        }

        [HttpDelete]
        public void destroy(int id, [FromBody] string username, [FromBody] string password)
        {
            if (_username == username && _password == password)
            {
                _repo.destroy(id);
            }
        }

        [HttpGet]
        public T get(int id)
        {
            return _repo.get(id);
        }

        [HttpPut]
        public void update(T record, [FromBody] string username, [FromBody] string password)
        {
            if (_username == username && _password == password)
            {
                _repo.update(record);
            }
        }
    }
}