﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using MimoAPI.Models;
using Microsoft.EntityFrameworkCore;
using MimoAPI.Controllers;

namespace MimoAPI.Controllers
{
    [Route("api/[controller]")]
    public class Achievement : API<Achievement>
    {

        public Achievement(IRepository<Achievement> repo)
        {
            base(repo);
        }
    }
}