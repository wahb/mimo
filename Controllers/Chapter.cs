﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using MimoAPI.Models;
using Microsoft.EntityFrameworkCore;
using MimoAPI.Controllers;

namespace MimoAPI.Controllers
{
    [Route("api/[controller]")]
    public class Chapter : API<Chapter>
    {

        public Chapter(IRepository<Chapter> repo)
        {
            base(repo);
        }
    }
}